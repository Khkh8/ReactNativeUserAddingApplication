// In App.js in a new project
import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import { View, Text, Button, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

let data = [{ name: 'Koira', age: 30 }, { name: 'Kissa', age: 40 }, { name: 'Henri', age: 22 }];
let placeholderIndex = 0

function DetailsScreen({ navigation, route }) {
  const [name, setName] = useState(route.params.name);
  const [age, setAge] = useState(JSON.stringify(route.params.age));
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ fontSize: 40, marginBottom: 40 }}>Details Screen</Text>
      <Text />
      <Text style={{ fontSize: 24 }}>Name: </Text>
      <TextInput
        style={{ height: 40, width: '70%', textAlign: 'center', fontSize: 18, backgroundColor: "#95afc0", marginTop: 5, marginBottom: 5 }}
        placeholder="name"
        onChangeText={text => setName(text)}
        defaultValue={name}
      />
      <Text style={{ fontSize: 24 }}>Age: </Text>
      <TextInput
        style={{ height: 40, width: '70%', textAlign: 'center', fontSize: 18, backgroundColor: "#95afc0", marginTop: 5, marginBottom: 5 }}
        placeholder="age"
        onChangeText={text => setAge(text)}
        defaultValue={age}
      />
      <View style={{ padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>
        <Button title="OK" onPress={() => navigation.navigate('Home', { type: "edit", name: name, age: parseInt(age) })} />
      </View>
      <View style={{ padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>
        <Button title="Cancel" onPress={() => navigation.navigate('Home', { type: "cancel" })} />
      </View>
      <View style={{ padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>
        <Button title="Delete" onPress={() => {
          navigation.navigate('Home', { type: "delete" })
        }} />
      </View>
    </View>
  );
}

function CreationScreen({ navigation, route }) {
  const [name, setName] = useState(route.params.name);
  const [age, setAge] = useState(JSON.stringify(route.params.age));
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ fontSize: 40, marginBottom: 40 }}>Creation Screen</Text>
      <Text style={{ fontSize: 24 }}>Name: </Text>
      <TextInput
        style={{ height: 40, width: '70%', textAlign: 'center', fontSize: 18, backgroundColor: "#95afc0", marginTop: 5, marginBottom: 5 }}
        placeholder="name"
        onChangeText={text => setName(text)}
      />
      <Text style={{ fontSize: 24 }}>Age: </Text>
      <TextInput
        style={{ height: 40, width: '70%', textAlign: 'center', fontSize: 18, backgroundColor: "#95afc0", marginTop: 5, marginBottom: 5 }}
        placeholder="age"
        onChangeText={text => setAge(text)}
      />
      <View style={{ padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>
        <Button title="OK" onPress={() => navigation.navigate('Home', { type: "insert", name: name, age: parseInt(age) })} />
      </View>
      <View style={{ padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>
        <Button title="Cancel" onPress={() => {
          navigation.navigate('Home', { type: "delete" })
        }} />
      </View>
    </View>
  );
}

function HomeScreen({ navigation, route }) {
  [list, setList] = useState(data);

  React.useEffect(() => {
    if (route.params) {
      try {
        if (route.params.type === "delete") {
          console.log("Poistamis testi + index = " + placeholderIndex);
          if (placeholderIndex == list.length - 1) { // Poistetaan viimeinen
            console.log("Poistetaan viimeinen");
            list.pop()
          }
          else {
            let placeholderObject = list.pop()
            list[placeholderIndex] = placeholderObject
            console.log(placeholderObject);
          }
        }
      }
      catch (e) {
        console.log(e + "\n ONGELMA KOHDASSA 1");
      }
      try {
        if (route.params.type === "insert" || route.params.type === "edit" || route.params.type === "delete") {
          setList(list.map((item, j) => {
            if (j === placeholderIndex && route.params.type !== "delete") {
              return { name: route.params.name, age: route.params.age };
            }
            else
              return item;
          }));
        }
      }
      catch (e) {
        console.log(e + "\n ONGELMA KOHDASSA 2");
      }
    }
  }, [route.params]);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ fontSize: 40, marginBottom: 40 }}>Home Screen</Text>
      <PrintList list={list} navigaatio={navigation} />
      <View style={{ padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>

        <Button
          title="add new"
          color="#f0f"
          onPress={() => {
            placeholderIndex = list.length
            list.push({ name: 'placeholder', age: 0 })
            navigation.navigate('Creation', list[placeholderIndex])
          }}
        />
      </View>
    </View>
  );
}

const Stack = createStackNavigator();

function PrintList(props) {
  return (
    <View>
      { props.list.map((item, index) => (
        <View key={Math.random()} style={{ width: 250, padding: 3, margin: 3, borderWidth: 1, backgroundColor: "white" }}>
          <View style={{ width: '100%' }}>
            <Text style={{ fontSize: 16, textAlign: 'center' }}>{"Name: " + item.name + " Age: " + item.age}</Text>
            </View>
          <View style={{ width: 60, alignSelf: 'center', margin: 5 }}>
            <Button title={"Edit"} onPress={async () => {
            placeholderIndex = index
            console.log("tarkistus1 " + placeholderIndex);
            console.log("tarkistus2 " + props.list[placeholderIndex]);
            await props.navigaatio.navigate('Details', props.list[placeholderIndex])
          }} />
          </View>
        </View>
      ))}
    </View>
  )
}
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen}
          options={{ title: 'Overview' }}
        />
        <Stack.Screen name="Creation" component={CreationScreen}
          options={{ title: 'Creation Screen' }}
        />
        <Stack.Screen name="Details" component={DetailsScreen}
          options={{ title: 'User details' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;