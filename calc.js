
const addition = (x,y) => {return(parseFloat(x)+parseFloat(y))}
const substraction = (x,y) => {return(parseFloat(x)-parseFloat(y))}
const multiplication = (x,y) => {return(parseFloat(x)*parseFloat(y))}
const division = (x,y) => {return(parseFloat(x)/parseFloat(y))}

export {addition, substraction, multiplication, division}