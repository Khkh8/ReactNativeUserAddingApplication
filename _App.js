/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
} from 'react-native';

import {
//   Header,
//   LearnMoreLinks,
  Colors,
//   DebugInstructions,
//   ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {addition, substraction, multiplication, division} from "./calc"

const App: () => React$Node = () => {
  const [op1, setOp1] = useState("0")
  const [op2, setOp2] = useState("0")
  const [prec, setprec] = useState("3")
  return (
    <>
      <SafeAreaView>
        <ScrollView contentContainerStyle={{ flex: 0, flexDirection: "column", justifyContent: "space-between", backgroundColor: "grey" }}>
          
          <View style={{ flex: 1, flexDirection: "row", justifyContent: "center"}}>
          <Text style={{ alignSelf: "center", backgroundColor: "magenta" }}>{"Operand1:    "}</Text>
          <TextInput style={{ alignSelf: "center", backgroundColor: "green" }} placeholder="Op1" defaultValue={op1} onChangeText={text => setOp1(parseFloat(text))}/>
          </View>
          
          <View style={{ flex: 1, flexDirection: "row", justifyContent: "center"}}>
          <Text style={{ alignSelf: "center", backgroundColor: "magenta" }}>{"Operand2:    "}</Text>
          <TextInput style={{ alignSelf: "center", backgroundColor: "green" }} placeholder="Op2" defaultValue={op2} onChangeText={text => setOp2(parseFloat(text))} />
          </View>

          <View style={{ flex: 1, flexDirection: "row", justifyContent: "center"}}>
          <Text style={{ alignSelf: "center", backgroundColor: "magenta" }}>{"Precision:    "}</Text>
          <TextInput style={{ alignSelf: "center", backgroundColor: "brown" }} placeholder="Op2" defaultValue={prec} onChangeText={text => text < 1 || text > 21 ? setprec(1) : setprec(parseInt(text))} />
          </View>

          <View>
            <Text>{"Addition:  " + addition(op1, op2)}</Text>
            <Text>{"Substraction:  " + substraction(op1, op2)}</Text>
            <Text>{"Multiplication:  " + multiplication(op1, op2)}</Text>
            <Text>{"Division:  " + division(op1, op2).toPrecision(prec)}</Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
